

## iGMMclust

A fast Rcpp implementation of the [Infinite Gaussian Mixture Model](https://www.seas.harvard.edu/courses/cs281/papers/rasmussen-1999a.pdf)


Original R implemntation available at https://github.com/alumbreras/DP-GMM



### Install

```
library(devtools)

install_bitbucket("weischenfeldt/igmmclust")
```


### Examples with iris

```
library(iGMMclust)
n_iter = 1000
zzz <- gibbs(t(iris[, 1:4]), kmeans(iris[, 1:4], 10)$cluster, n_iter)
   |++++++++++++++++++++++++++++++++++++++++++++++++++| 100% elapsed = 16s
```

![iris100iter](figures/iter_iris.gif)

```
table(iris[, 5], zzz$cluster())
```

|            | 3 | 6 | 7 |
|------------|---|---|---|
|  setosa    |  0|  0| 50|
|  versicolor|  4| 46|  0|
|  virginica | 50|  0|  0|


```
pairs(iris, col = zzz$cluster(), lower.panel=NULL, las = 1)
```

### Check clustering performance

```
library(clues)
 
adjustedRand(zzz$cluster(), as.numeric(iris[,5]))
```
 
|    Rand   |     HA    |     MA    |     FM    |  Jaccard  |
|-----------|-----------|-----------|-----------|-----------|
| 0.9656376 | 0.9222414 | 0.9232818 | 0.9478708 | 0.9009032 |
